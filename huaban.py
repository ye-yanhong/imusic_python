# -*- coding: utf-8 -*-
# @Author: Anderson
# @Date:   2019-11-14 16:30:55
# @Last Modified by:   Anderson
# @Last Modified time: 2019-12-30 15:32:39
from makerbean import web_crawler_bot as wbot
from makerbean import excel_bot as ebot
from makerbean import data_analysis_bot as dbot
from makerbean import pdf_bot as pbot

my_cookie='sid=s%3AEUC0QroqfDNngFS7LBUy3md--t1esmQH.zH1xpyc5nbKj%2Fk2Exsq0VcD%2Bk8nCJ7Cnr8T5LStnTfE; wft=1; _f=iVBORw0KGgoAAAANSUhEUgAAADIAAAAUCAYAAADPym6aAAACn0lEQVRYR%2B1WTWgTURCelx6EtkQ8GARpCwYPRkXBpqV42K2CBJWChwYsWAXNSQQPggq2BwUTwUPEk0REI0RQQfBiEWt3QSg1HjxoFSSiFqT4B4rmoJjnN8%2FdELfJJqsbRW1g2Mmb703me%2FO92QiSUpLbRwjhGv9NwfS7tGudgom0FouUSSRUSYeTSbocj1MhHKZEJkPFtjZXIsjeim0Z2DiA55rFqyEi%2FOOaaaoaTE37wacGOgIyK7B1FYhcmSdS5wQ8dcTQ9XK62NgYDWezNJTL7cbiddgF2AELcAPPLtgUbCtsMewIbMix5sTx%2FjWwmI1DF9800kVPRJzSehUK0XQkEmfJQD6DiD%2BA%2Fwh%2BCv5BR9HbEDuOmMZ54Jt1cOV8f4QIfrQPVrAK3Qf%2FktURdUdsIlgL18H5T6TW1DqUStGLzk6WT691aifwZLkYFaeYg38Sds2SG3%2FnERitgpvF2nrYLth5JspYHECxXlcallbNRA1MrXpF%2BBGfJ1I%2BxX%2B5I9Kc2kBSZDGbgiC8FyNqJ0nqYV%2Fo0YtOKUnj3kMiGQF%2BWujdK2tJzQ3nlJb2chk9Db6lmfb3Kp36i%2BKq4Rodkcbdo9i%2BFoUPKF8GgqK%2Fe%2F9cEgq3Bbh10szfohI9E%2F3RPV5xNpGOjwtp08xytf1mxxMfiKiuBI6RKI1QqWUAzw9C7xmdW2AeL1N5n2OV5L3iKjvCZPpmu2hyyfNfJ8KF4ITvQFJn4A6CSFpovbfdC8zvQHwzurPdK665RCbyZyHOEIr6VK04Rdb4Gzpi8AnLJCR2tdr9%2BE7E3zvCOZsgLb4nLadRbqratLLloy65pI321JKqkzIGG66Uoxvu1OrJcZsEX%2Fb2Lwvoc%2BArTSwt0ONFr39%2BavnxtvaS4795s38DwkXZKPKtM9cAAAAASUVORK5CYII%3D%2CWin32.1920.1200.24; _uab_collina=158322629910780488727645; Hm_lvt_d4a0e7c3cd16eb58a65472f40e7ee543=1583226299; Hm_lpvt_d4a0e7c3cd16eb58a65472f40e7ee543=1583226299; __asc=1ea14ff61709fa42419e10154c5; __auc=1ea14ff61709fa42419e10154c5; UM_distinctid=1709fa42428177-07bf68997c1cde-3a36550e-232800-1709fa424293c4; _cnzz_CV1256903590=is-logon%7Clogged-out%7C1583226300438; CNZZDATA1256903590=1062482401-1583224704-%7C1583224704'
wbot.set_cookie(my_cookie)
for page in range(10):
    data = wbot.get_huaban('音符',page)
    for img in data:
        url=img['url']
        name=img['name']
        wbot.download_image(url,name,'images_folder')
    
    
##data = wbot.get_huaban('设计',0)
##for img in data:
##    url=img['url']
##    name=img['name']
##    wbot.download_image(url,name,'images_folder')
##    print(img)
##    ebot.add_row(img)
##ebot.save('huaban')

