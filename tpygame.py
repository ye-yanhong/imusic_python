
# 导入pygame模块
import pygame
import time

# 初始化pygame
pygame.init()
width = 1244
height = 689
speed = 2
# 创建舞台,利用Pygame中的display模块，来创建窗口
screen = pygame.display.set_mode((width, height), 0, 32)
# 设置窗口标题
pygame.display.set_caption("Hello PyGame")

# 我的cat.png和cat.py文件在同一个文件夹下面
# 所以可以直接这样加载图片的
# laod函数加载图片
catl4 = pygame.image.load("cat_L_4fen.png")
catl8 = pygame.image.load("cat_L_8fen.png")
catr4 = pygame.image.load("cat_R_4fen.png")
catr8 = pygame.image.load("cat_R_8fen.png")
birdl4=pygame.image.load("bird_L_4fen.png")
birdr4=pygame.image.load("bird_R_4fen.png")
birdl8=pygame.image.load("bird_L_8fen.png")
birdr8=pygame.image.load("bird_R_8fen.png")
hline=pygame.image.load("hline.png")
cat_x, cat_y = width-240, 0  # y猫的坐标
#bird_x, bird_y = width+200, 0  # y猫的坐标
hline_x, hline_y = 200, 0 
#cat_x, cat_y = 0, 120  # 猫的坐标
h_direction = 1  # 水平方向

def myfunc(animal,animal_x,animal_y):
    screen.blit(animal, (animal_x, animal_y))
    animal_x -= speed * h_direction
#    pygame.display.update()
while 1:
    for event in pygame.event.get():
        # 这段程序大家可能比较费解，实际上是检测quit事件，实际讲课中让学生直接模仿即可，时间足够也可以讲明白
        if event.type == pygame.QUIT:
            pygame.quit()

    pygame.mixer.music.load("/Users/mc/Downloads/ds_china.wav")
    pygame.mixer.music.set_volume(11111111111.0)
    # blit函数的作用是把加载的图片放到舞台的（cat_x, cat_y）坐标的位置
    screen.fill((255,255,255))
    screen.blit(hline, (hline_x, hline_y))
    myfunc(catl4, cat_x-200, 0)
    myfunc(birdl4, cat_x+1200, 0)
    myfunc(birdr8, cat_x+1500, 0)
    myfunc(catr4, cat_x+2200, 0)
    myfunc(birdr4, cat_x+2600, 0)
    myfunc(catr8, cat_x+3800, 0)
    #screen.blit(cat, (cat_x, cat_y))
    #screen.blit(bird, (bird_x, bird_y))
    # 这样就实现了会移动的猫
    cat_x -= speed * h_direction
    #bird_x -= speed * h_direction
    pygame.display.update()

    # 如果猫的坐标超出了640，就让小猫反向
    # 如果猫的坐标小于了0，也让小猫反向，这样就实现了碰到墙壁反弹的效果
#    h_direction = -h_direction
#    if cat_x >= width-248:
#        h_direction = -h_direction
#    elif cat_x < 0:
#        h_direction = -h_direction
    pygame.display.update()

